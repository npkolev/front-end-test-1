# Readme

## Setup guides

### If you're comfortable with git...

1. Fork it
2. Clone it
3. Commit changes to your own fork
4. Send link of your repo, and we will clone (or download) it for review.

### If you're not comfortable with git...

1. Click "Downloads"
2. Click "Download repository"
3. Make changes
4. Once finished, zip it up and send via email

## Test instructions

1. Add a login form to the provided HTML page. The login form should contain an email and password field.

2. Use semantic html (not XHTML) that is accessible (use labels etc).

3. Style it so that it works responsively - please don't put too much effort into this, a few lines of code should do it.

4. Please validate the form with Javascript. We are looking for you to encapsulate all the functionality inside a constructor utilising methods on the prototype as appropriate.  We are not looking for inline/obtrusive javascript.  We also don't want to see more than a single global variable. **Note:** If you're not comfortable working with a constructor/prototype then please solve the problem in whatever way you are comfortable with, but add a note explaining why you've chosen a different approach.

5. The validation only needs to check for empty fields.  The errors need to display in red below each field that has an error. The error messages should be as follows: "Damn, please enter your email address." and "Oops, we just need your password too."

6.  When validation succeeds, the form should submit to http://google.co.uk

7.  Please use external css and js files as provided.

8.  We are not looking for the use of **lots** of libraries.  We want to see how you would code this. However, if you would use a library for certain bits of it, feel free, but explain why.